package main.array;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class AList1 {

    private int[] data;
    private int capacity;
    private int size;

    public AList1(int[] array) {

        if (array == null) {
            throw new IllegalArgumentException();
        }
        int[] buffer = Arrays.copyOf(array, array.length);
        this.data = buffer;
        this.capacity = buffer.length;
        this.size = buffer.length;
    }

    public AList1() {
        this(10);
    }

    public AList1(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException();
        }
        this.capacity = capacity;
        this.data = new int[this.capacity];
        this.size = 0;
    }

    public void clear() {
        Arrays.fill(data, 0);
        size = 0;
    }

    public int size() {
        return this.size;
    }

    public int get(int index) {
        if (index > (this.size - 1)) {
            throw new IndexOutOfBoundsException();
        }
        return data[index];
    }

    public boolean add(int value) {
        return add(size, value);
    }

    public boolean add(int index, int value) {
        if (index > size) {
            throw new IndexOutOfBoundsException();
        }
        if (size >= capacity) {
            resize();
        }
        System.arraycopy(data, index, data, index + 1, size - index);
        data[index] = value;
        size++;
        return true;
    }

    public int remove(int value) {
        int indexOfValue = -1;
        for (int i = 0; i < size; i++) {
            if (data[i] == value) {
                indexOfValue = i;
            }
        }
        if (indexOfValue == -1) {
            throw new NoSuchElementException();
        } else {
            return removeByIndex(indexOfValue);
        }
    }

    public int removeByIndex(int index) {
        int element = get(index);
        System.arraycopy(data, index + 1, data, index, size - index);
        data[size] = 0;
        size--;
        return element;
    }

    public boolean contains(int value) {
        for (int i = 0; i < size; i++) {
            if (data[i] == value) {
                return true;
            }
        }
        return false;
    }

    public boolean set(int index, int value) {
        if (index >= size) {
            return false;
        }
        data[index] = value;
        return true;
    }

    public void print() {
        System.out.println(generateLine());
    }

    public int[] toArray() {
        return Arrays.copyOf(data, size);
    }

    public boolean removeAll(int[] arr) {
        if (arr == null) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            if (contains(arr[i])) {
                remove(arr[i]);
            }
        }
        return true;
    }

    private void resize() {
        int newLength = ((capacity * 3) / 2) + 1;
        int[] newArray = new int[newLength];
        System.arraycopy(data, 0, newArray, 0, size);
        data = newArray;
        capacity = newLength;
    }

    private String generateLine() {
        if (data == null) {
            return "null";
        }


        int iMax = size - 1;
        if (iMax == -1) {
            return "[]";
        }

        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(data[i]));
            if (i == iMax) {
                return b.append(']').toString();
            }
            b.append(", ");
        }
    }
}
