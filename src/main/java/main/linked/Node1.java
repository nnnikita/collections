package main.linked;

public class Node1<T> {

    public T object;
    public Node1<T> next;

    public Node1(T object, Node1<T> next) {
        this.object = object;
        this.next = next;
    }
}
