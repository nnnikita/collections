package main.linked;

import main.interfaces.IList;
import java.util.Arrays;

public class LList2<T> implements IList<T> {

    private Node2<T> first;
    private Node2<T> last;
    private int size;

    public LList2() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    public LList2(T[] array) {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        T[] buffer = Arrays.copyOf(array, array.length);
        for (int i = 0; i < buffer.length; i++) {
            add(buffer[i]);
        }
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public T get(int index) {
        if ((size < 1) || (size <= index) || (index < 0)) {
            throw new IndexOutOfBoundsException();
        }
        Node2<T> x = first;
        if (index == 0) {
            return x.object;
        }
        for (int i = 0; i < index; i++) {
            x = x.next;
        }

        return x.object;
    }

    @Override
    public boolean add(T value) {
        return addLast(value);
    }

    @Override
    public boolean add(int index, T value) {
        if (index < 0 || index > size) {
            return false;
        }

        if (index == size) {
            return addLast(value);
        }

        if (index == 0) {
            return addFirst(value);
        }

        Node2<T> current = getNodeInIndex(index - 1);
        Node2<T> afterCurrent = current.next;
        Node2<T> newNode = new Node2<>(value, current, afterCurrent);
        current.next = newNode;
        afterCurrent.previous = newNode;
        size++;
        return true;
    }

    @Override
    public T remove(T value) {
        int index = indexOf(value);
        return removeByIndex(index);
    }

    @Override
    public T removeByIndex(int index) {
        if (index < 0 || index >= size) {
            return null;
        }

        T objInIndex;

        if (index == 0) {
            objInIndex = first.object;
            if (size > 1) {
                first = first.next;
                first.previous = null;
            } else {
                first = null;
                last = null;
            }
            size--;
            return objInIndex;
        }

        if (index == size - 1) {
            objInIndex = last.object;
            last = last.previous;
            last.next = null;
            size--;
            return objInIndex;
        }

        Node2<T> nodeInIndex = getNodeInIndex(index);
        Node2<T> previousNode = nodeInIndex.previous;
        Node2<T> nextNode = nodeInIndex.next;

        previousNode.next = nextNode;
        nextNode.previous = previousNode;
        size--;
        return nodeInIndex.object;
    }

    @Override
    public boolean contains(T value) {
        return indexOf(value) != -1;
    }

    @Override
    public boolean set(int index, T value) {
        if (index < 0 || index >= size || size == 0) {
            return false;
        }
        getNodeInIndex(index).object = value;
        return true;
    }

    @Override
    public void print() {
        System.out.println(generateLine());
    }

    @Override
    public T[] toArray() {
        T[] rez = (T[]) new Object[size];
        if (size < 1) {
            return rez;
        }
        Node2<T> x = first;
        for (int i = 0; i < size; i++) {
            rez[i] = x.object;
            x = x.next;
        }
        return rez;
    }

    @Override
    public boolean removeAll(T[] arr) {
        if (arr == null) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            remove(arr[i]);
        }
        return true;
    }


    public T getFirst() {
        if (first == null) {
            return null;
        }
        return first.object;
    }

    public T getLast() {
        if (last == null) {
            return null;
        }
        return last.object;
    }

    private boolean addLast(T value) {
        Node2<T> l = last;
        Node2<T> newNode = new Node2<>(value, l, null);
        last = newNode;
        if (l == null) {
            first = newNode;
        } else {
            l.next = newNode;
        }
        size++;
        return true;
    }

    private Node2<T> getNodeInIndex(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        Node2<T> nodeInIndex = first;

        for (int i = 0; i < index; i++) {
            nodeInIndex = nodeInIndex.next;
        }

        return nodeInIndex;
    }

    private boolean addFirst(T value) {
        Node2<T> newNode = new Node2<>(value, null, first);
        first = newNode;
        size++;
        return true;
    }

    private int indexOf(T obj) {
        int index = 0;
        if (obj == null) {
            for (Node2<T> x = first; x != null; x = x.next) {
                if (x.object == null) {
                    return index;
                }
                index++;
            }
        } else {
            for (Node2<T> x = first; x != null; x = x.next) {
                if (obj.equals(x.object)) {
                    return index;
                }
                index++;
            }
        }
        return -1;
    }

    private String generateLine() {
        int iMax = size - 1;
        if (iMax == -1) {
            return "[]";
        }

        Node2<T> x = first;

        StringBuilder b = new StringBuilder();
        b.append('[');

        for (int i = 0; ; i++) {
            b.append(String.valueOf(x.object));
            if (i == iMax) {
                return b.append(']').toString();
            }
            b.append(", ");
            x = x.next;
        }
    }

}
