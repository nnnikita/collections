package main.linked;

public class Node2<T> {

    public T object;
    public Node2<T> previous;
    public Node2<T> next;

    public Node2(T object, Node2<T> previous, Node2<T> next) {
        this.object = object;
        this.previous = previous;
        this.next = next;
    }
}
