package array;

import main.array.AList2;
import main.interfaces.IList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.NoSuchElementException;
import static org.junit.jupiter.api.Assertions.*;


class AList2Test {

    private final IList<String> cut = new AList2<>();


//    ****************************************************************
    static Arguments[] constructorsTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new AList2<String>(), new String[]{}, 0),
                Arguments.arguments(new AList2<String>(0), new String[]{}, 0),
                Arguments.arguments(new AList2<String>(5), new String[]{}, 0),
                Arguments.arguments(new AList2<String>(new String[]{"1", null, "2"}), new String[]{"1", null, "2"}, 3),
        };
    }


    @ParameterizedTest
    @MethodSource("constructorsTestArgs")
    void constructorTest(IList<String> cut, String[] expected, int expSize) {
        Object[] actual = cut.toArray();
        int actSize = cut.size();

        assertEquals(expSize, actSize);
        assertArrayEquals(expected, actual);
    }

    @Test
    void constructorsExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> new AList2<String>(-2));

        assertThrows(IllegalArgumentException.class, () -> new AList2<String>(null));
    }


//    ****************************************************************
    static Arguments[] addNoIndexAndGetTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5),
                Arguments.arguments(12)
        };
    }

    @ParameterizedTest
    @MethodSource("addNoIndexAndGetTestArgs")
    void addNoIndexAndGetTest(int countOfAdd) {
        String expected = "";
        for (int i = 0; i < countOfAdd; i++) {
            expected = String.valueOf(i);
            cut.add(expected);
        }

        String actual = cut.get(cut.size() - 1);

        assertEquals(expected, actual);
        assertEquals(countOfAdd, cut.size());
    }

//    ****************************************************************
    static Arguments[] toArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(2, new String[]{"0", "1"}),
                Arguments.arguments(5, new String[]{"0", "1", "2", "3", "4"}),
                Arguments.arguments(0, new String[]{}),
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(int countAdd, String[] expected) {
        for (int i = 0; i < countAdd; i++) {
            cut.add(String.valueOf(i));
        }

        Object[] actual = cut.toArray();

        assertArrayEquals(expected, actual);
    }

//    ****************************************************************
    @Test
    void clearTest() {
        int expSize = 0;
        cut.add("A");
        cut.add("B");
        cut.add("C");
        cut.add("D");

        cut.clear();

        assertEquals(expSize, cut.size());
        assertThrows(IndexOutOfBoundsException.class, () -> cut.get(0));
    }
//    ****************************************************************

    static Arguments[] addIndexTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1, "AAA"),
                Arguments.arguments(5, 5, "AAA"),
                Arguments.arguments(5, 1, null),

        };
    }

    @ParameterizedTest
    @MethodSource("addIndexTestArgs")
    void addIndexTest(int countOfAdd, int index, String expected) {

        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        cut.add(index, expected);
        String actual = cut.get(index);

        assertEquals(expected, actual);
        assertEquals(countOfAdd + 1, cut.size());
    }

    static Arguments[] addIndexExceptionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 6, "AAA"),
                Arguments.arguments(5, -1, "AAA"),
        };
    }

    @ParameterizedTest
    @MethodSource("addIndexExceptionTestArgs")
    void addIndexExceptionTest(int countOfAdd,  int index, String expected) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        assertThrows(IndexOutOfBoundsException.class, () -> cut.add(index, expected));

    }

//    ****************************************************************
    static Arguments[] containsTestArgs() {
        return new Arguments[]{
               Arguments.arguments(true, "2", 5),
               Arguments.arguments(false, "6", 5),
               Arguments.arguments(false, "0", 0),
               Arguments.arguments(false, null, 0),
               Arguments.arguments(true, null, 5),
        };
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(boolean expected, String value, int countOfAdd) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(null);
            cut.add(String.valueOf(i));
        }

        boolean actual = cut.contains(value);

        assertEquals(expected, actual);
    }

//    ****************************************************************

    static Arguments[] removeByIndexTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1),
                Arguments.arguments(5, 4),
                Arguments.arguments(5, 0),
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int countOfAdd, int index) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }
        int expSize = cut.size() - 1;
        String expRemovedValue = String.valueOf(index);


        String actRemovedValue = cut.removeByIndex(index);
        int actSize = cut.size();


        assertEquals(expRemovedValue, actRemovedValue);
        assertEquals(expSize, actSize);
    }

    static Arguments[] removeByIndexExceptionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 6),
                Arguments.arguments(5, -1),
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexExceptionTestArgs")
    void removeByIndexExceptionTest(int countOfAdd,  int index) {
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        assertThrows(IndexOutOfBoundsException.class, () -> cut.removeByIndex(index));
    }

    @Test
    void removeByIndexNullTest() {
        cut.add("0");
        cut.add(null);
        cut.add("1");
        int expSize = cut.size() - 1;
        String expRemovedValue = null;

        String actRemovedValue = cut.removeByIndex(1);
        int actSize = cut.size();

        assertEquals(expRemovedValue, actRemovedValue);
        assertEquals(expSize, actSize);
    }

//    ****************************************************************

    static Arguments[] removeTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, "1"),
                Arguments.arguments(5, "4"),
                Arguments.arguments(5, null),
        };
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int countOfAdd, String value) {
        cut.add(null);
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }
        int expSize = cut.size() - 1;


        String actRemovedValue = cut.remove(value);
        int actSize = cut.size();


        assertEquals(value, actRemovedValue);
        assertEquals(expSize, actSize);
    }

    @Test
    void removeExceptionTest() {

        assertThrows(NoSuchElementException.class, () -> cut.remove("AAA"));

        cut.add("BBB");

        assertThrows(NoSuchElementException.class, () -> cut.remove("AAA"));
    }

//    ****************************************************************

    static Arguments[] setTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 1, "AAA", true),
                Arguments.arguments(5, 0, "AAA", true),
                Arguments.arguments(5, 1, null, true),
        };
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int countOfAdd, int index, String value, boolean expected) {
        cut.add(null);
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }
        int expSize = cut.size();
        boolean actualRez = cut.set(index, value);


        String actual = cut.get(index);
        int actSize = cut.size();

        assertEquals(expSize, actSize);
        assertEquals(value, actual);
        assertEquals(expected, actualRez);
    }

    @Test
    void setFalseTest() {

        assertFalse(cut.set(2, "AAA"));

        cut.add("Z");
        cut.add("X");
        cut.add("C");

        assertFalse(cut.set(3, "AAA"));
    }
//    ****************************************************************

    static Arguments[] removeAllTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, new String[]{"3", "1"}, new String[]{null, "0", "2", "4"}),
                Arguments.arguments(5, new String[]{null, "1"}, new String[]{"0", "2", "3", "4"}),
                Arguments.arguments(5, new String[]{"6", "1"}, new String[]{null, "0", "2", "3", "4"}),
                Arguments.arguments(5, new String[]{"6", "99"}, new String[]{null, "0", "1", "2", "3", "4"}),
                Arguments.arguments(5, new String[]{null, null, "99"}, new String[]{"0", "1", "2", "3", "4"}),
                Arguments.arguments(5, new String[]{}, new String[]{null, "0", "1", "2", "3", "4"}),
                Arguments.arguments(5, null, new String[]{null, "0", "1", "2", "3", "4"}),
        };
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(int countOfAdd, String[] removeValues, String[] expected) {
        cut.add(null);
        for (int i = 0; i < countOfAdd; i++) {
            cut.add(String.valueOf(i));
        }

        cut.removeAll(removeValues);

        assertArrayEquals(expected, cut.toArray());
    }

    @Test
    void removeAllRepeatedValueTest() {
        cut.add(null);
        cut.add(null);
        cut.add("A");
        cut.add("B");
        cut.add("B");
        cut.add("C");
        String[] expected = new String[]{null, "B", "C"};

        cut.removeAll(new String[]{ null, "A", "B"});

        assertArrayEquals(expected, cut.toArray());
    }
}